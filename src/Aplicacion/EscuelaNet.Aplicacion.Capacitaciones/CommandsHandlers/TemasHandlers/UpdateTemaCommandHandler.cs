﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.TemaCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers
{
    public class UpdateTemaCommandHandler
        : IRequestHandler<UpdateTemaCommand, CommandRespond>
    {
        private ITemaRepository _temaRepository;
        public UpdateTemaCommandHandler(ITemaRepository temaRepository)
        {
            _temaRepository = temaRepository;

        }


        public Task<CommandRespond> Handle(UpdateTemaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {

                    var tema = _temaRepository.GetTema(request.Id);
                    tema.Nombre = request.Nombre;
                    tema.Nivel = request.Nivel;

                    _temaRepository.Update(tema);
                    _temaRepository.UnitOfWork.SaveChanges();


                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);

                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "El nombre está vacio.";
                return Task.FromResult(responde);
            }
        }
    }
}
