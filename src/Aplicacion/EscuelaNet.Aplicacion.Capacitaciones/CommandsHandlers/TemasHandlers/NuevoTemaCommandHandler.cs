﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands.TemaCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers
{
    public class NuevoTemaCommandHandler
        : IRequestHandler<NuevoTemaCommand, CommandRespond>
    {
        private ITemaRepository _temaRepositorio;
        public NuevoTemaCommandHandler(ITemaRepository temaRepositorio)
        {
            _temaRepositorio = temaRepositorio;
        }
        public Task<CommandRespond> Handle(NuevoTemaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {
                    _temaRepositorio.Add(new Tema(request.Nombre, request.Nivel));
                    _temaRepositorio.UnitOfWork.SaveChanges();
                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);

                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "El nombre está vacio.";
                return Task.FromResult(responde);
            }
        }
    }
}
