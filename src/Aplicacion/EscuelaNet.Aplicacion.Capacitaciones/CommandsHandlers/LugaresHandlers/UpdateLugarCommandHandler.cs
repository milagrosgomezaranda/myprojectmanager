﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.LugaresHandlers
{
    public class UpdateLugarCommandHandler : IRequestHandler<UpdateLugarCommand, CommandRespond>
    {
        private ILugarRepository _lugarRepository;
        public UpdateLugarCommandHandler(ILugarRepository lugarRepository)
        {
            _lugarRepository = lugarRepository;
        }
        public Task<CommandRespond> Handle(UpdateLugarCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (request.Capacidad > 0)
            {
                if (string.IsNullOrEmpty(request.Depto))
                {
                    request.Depto = " ";
                }

                if (string.IsNullOrEmpty(request.Piso))
                {
                    request.Piso = " ";
                }

                try
                {
                    var lugar = _lugarRepository.GetLugar(request.Id);
                    lugar.Calle = request.Calle;
                    lugar.Capacidad = request.Capacidad;
                    lugar.Depto = request.Depto;
                    lugar.Localidad = request.Localidad;
                    lugar.Numero = request.Numero;
                    lugar.Pais = request.Pais;
                    lugar.Piso = request.Piso;
                    lugar.Provincia = request.Provincia;


                    _lugarRepository.Update(lugar);
                    _lugarRepository.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);


                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "La capacidad debe ser mayor a 0.";
                return Task.FromResult(responde);
            }
        }
    }
}
