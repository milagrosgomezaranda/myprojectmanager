﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.InstructoresHandlers
{
    public class DeleteInstructorCommandHandler : IRequestHandler<DeleteInstructorCommand, CommandRespond>
    {
        private IInstructorRepository _instructorRepository;
        public DeleteInstructorCommandHandler(IInstructorRepository instructorRepository)
        {
            _instructorRepository = instructorRepository;
        }

        public Task<CommandRespond> Handle(DeleteInstructorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var instructor = _instructorRepository.GetInstructor(request.Id);
                _instructorRepository.Delete(instructor);
                _instructorRepository.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);
            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = "ERROR";
                return Task.FromResult(responde);
            }
        }
    }
}
