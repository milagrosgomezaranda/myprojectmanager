﻿using Dapper;
using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryServices
{
    public class LugaresQuery : ILugaresQuery
    {
        private string _connectionString;
        public LugaresQuery(string connectionString)
        {
            _connectionString = connectionString;
        }

        public LugarQueryModel GetLugar(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<LugarQueryModel>(
                    @"
                    SELECT t.IDLugar as Id,
                    t.Capacidad as Capacidad,
                    t.Calle as Calle,
                    t.Numero as Numero,
                    t.Depto as Depto,
                    t.Piso as Piso,
                    t.Localidad as Localidad,
                    t.Provincia as Provincia,
                    t.Pais as Pais
                    FROM Lugares as t
                    WHERE IDLugar = @id
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<LugarQueryModel> ListLugares()
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                return connection
                   .Query<LugarQueryModel>(
                   @"
                    SELECT t.IDLugar as Id,
                    t.Capacidad as Capacidad,
                    t.Calle as Calle,
                    t.Numero as Numero,
                    t.Depto as Depto,
                    t.Piso as Piso,
                    t.Localidad as Localidad,
                    t.Provincia as Provincia,
                    t.Pais as Pais
                    FROM Lugares as t
                    "
                   ).ToList();
            }
        }
    }
}
