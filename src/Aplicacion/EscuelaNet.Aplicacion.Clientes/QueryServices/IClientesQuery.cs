﻿using EscuelaNet.Aplicacion.Clientes.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.QueryServices
{
    public interface IClientesQuery
    {
        ClientesQueryModel GetCliente(int id);

        List<ClientesQueryModel> ListCliente();
    }
}
