﻿using EscuelaNet.Aplicacion.Clientes.Commands.UnidadCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.UnidadCommandHandler
{
    public class DeleteUnidadCommandHandler :
        IRequestHandler<DeleteUnidadCommand, CommandRespond>
    {
        private IClienteRepository _clienteRepositorio;

        public DeleteUnidadCommandHandler(IClienteRepository clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }

        public Task<CommandRespond> Handle(DeleteUnidadCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var unidad = _clienteRepositorio.GetUnidadDeNegocio(request.IdUnidad);
                _clienteRepositorio.DeleteUnidadDeNegocio(unidad);
                _clienteRepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);
            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}
