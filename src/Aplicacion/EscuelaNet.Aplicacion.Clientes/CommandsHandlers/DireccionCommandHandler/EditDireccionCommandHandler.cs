﻿using EscuelaNet.Aplicacion.Clientes.Commands.DireccionCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.DireccionCommandHandler
{
    public class EditDireccionCommandHandler :
        IRequestHandler<EditDireccionCommand, CommandRespond>
    {
        private IClienteRepository _clienteRepositorio;

        public EditDireccionCommandHandler(IClienteRepository clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }
        public Task<CommandRespond> Handle(EditDireccionCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.Domicilio))
            {
                try
                {
                    var unidadDeNegocio = _clienteRepositorio.GetUnidadDeNegocio(request.IdUnidad);

                    unidadDeNegocio.Direcciones.Where(d => d.ID == request.IdDireccion).First().Domicilio = request.Domicilio;
                    unidadDeNegocio.Direcciones.Where(d => d.ID == request.IdDireccion).First().Localidad = request.Localidad;
                    unidadDeNegocio.Direcciones.Where(d => d.ID == request.IdDireccion).First().Provincia = request.Provincia;
                    unidadDeNegocio.Direcciones.Where(d => d.ID == request.IdDireccion).First().Pais = request.Pais;

                    _clienteRepositorio.Update(unidadDeNegocio.Cliente);
                    _clienteRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Complete todos los campos.";
                return Task.FromResult(responde);
            }
        }
    }
}
