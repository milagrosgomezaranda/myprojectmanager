﻿using EscuelaNet.Aplicacion.Clientes.Commands.SolicitudCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.SolicitudCommandHandler
{
    class LinkUnidadCommandHandler :
        IRequestHandler<LinkUnidadCommand, CommandRespond>
    {
        private IClienteRepository _clientesRepositorio;
        private ISolicitudRepository _solicitudesRepositorio;

        public LinkUnidadCommandHandler(IClienteRepository clientesRepositorio,
            ISolicitudRepository solicitudRepository)
        {
            _clientesRepositorio = clientesRepositorio;
            _solicitudesRepositorio = solicitudRepository;
        }

        public Task<CommandRespond> Handle(LinkUnidadCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var solicitudBuscada = _solicitudesRepositorio.GetSolicitud(request.IDSolicitud);
                var unidadBuscada = _clientesRepositorio.GetUnidadDeNegocio(request.IDUnidad);

                unidadBuscada.AgregarSolicitud(solicitudBuscada);
                _clientesRepositorio.Update(unidadBuscada.Cliente);
                _clientesRepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}
