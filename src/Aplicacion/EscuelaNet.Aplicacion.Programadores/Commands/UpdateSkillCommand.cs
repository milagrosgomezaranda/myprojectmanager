﻿using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.Commands
{
    public class UpdateSkillCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public Experiencia Grados { get; set; }

    }
}
