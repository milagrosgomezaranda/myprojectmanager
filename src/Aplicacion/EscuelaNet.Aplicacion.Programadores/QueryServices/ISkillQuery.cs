﻿using EscuelaNet.Aplicacion.Programadores.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.QueryServices
{
    public interface ISkillQuery
    {
        SkillQueryModel GetSkill(int id);
        List<SkillQueryModel> ListSkill();
    }
}
