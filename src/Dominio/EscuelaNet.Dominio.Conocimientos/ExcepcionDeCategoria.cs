﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public class ExcepcionDeCategoria : Exception
    {
        public ExcepcionDeCategoria(string message) : base(message)
        { }

    }
}
