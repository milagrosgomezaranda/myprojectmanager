﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public class Archivo : Entity
    {
        public string Nombre { set; get; }
        public string Tipo { set; get; }
        public string Descripcion { set; get; }

        public Archivo (String nombre, String tipo, String descripcion)
        {
            this.Nombre = nombre;
            this.Tipo = tipo;
            this.Descripcion = descripcion;
        }



    }
}
