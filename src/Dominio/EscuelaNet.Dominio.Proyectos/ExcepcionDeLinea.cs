﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public class ExcepcionDeLinea : Exception
    {
        public ExcepcionDeLinea(string message) : base(message)
        { }
    }
}
