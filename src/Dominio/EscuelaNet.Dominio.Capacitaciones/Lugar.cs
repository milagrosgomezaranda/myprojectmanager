﻿  using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Lugar : Entity, IAggregateRoot
    {
        public int Capacidad { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Depto { get; set; }
        public string Piso { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string Pais { get; set; }

        private Lugar()
        {
            
        }

        public Lugar(int capacidad,string calle, string numero, string depto, string piso,
            string localidad, string provincia, string pais) : this()
        {
            
                this.Capacidad = capacidad;
                this.Calle = calle ?? throw new System.ArgumentNullException(nameof(calle));
                this.Numero = numero ?? throw new System.ArgumentNullException(nameof(numero));
                this.Depto = depto;
                this.Piso = piso;
                this.Localidad = localidad ?? throw new System.ArgumentNullException(nameof(localidad));
                this.Provincia = provincia ?? throw new System.ArgumentNullException(nameof(provincia)); ;
                this.Pais = pais ?? throw new System.ArgumentNullException(nameof(pais));

        }
    }
}
