﻿using EscuelaNet.Dominio.Conocimientos;
using System.Data.Entity.ModelConfiguration;

namespace EscuelaNet.Infraestructura.Conocimientos.EntityTypeConfigurations
{
    public class CatagoriaEntityTypeConfiguration : EntityTypeConfiguration<Categoria>
    {
        public CatagoriaEntityTypeConfiguration()
        {
            this.ToTable("Categoria");
            this.HasKey<int>(c => c.ID);
            this.Property(c => c.ID).
                HasColumnName("IDCategoria");
            this.Property(c => c.Nombre).
                IsRequired();
            this.Property(c => c.Descripcion).
                IsRequired();
            //Categoria tiene muchos Conocimientos
            this.HasMany<Conocimiento>(c => c.Conocimientos)
               .WithRequired(con => con.Categoria)
               .HasForeignKey<int>(con => con.IDCategoria);

        }
    }
}