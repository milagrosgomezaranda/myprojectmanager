﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos.EntityTypeConfigurations
{
    public class AsesorEntityTypeConfiguration : EntityTypeConfiguration<Asesor>
    {
        public AsesorEntityTypeConfiguration()
        {
            this.ToTable("Asesor");
            this.HasKey<int>(a => a.ID);
            this.Property(a => a.ID)
                .HasColumnName("IDAsesor");
            this.Property(a => a.Nombre)
                 .IsRequired();
            this.Property(a => a.Apellido)
                .IsRequired();
            this.Property(a => a.Disponibilidad)
                .IsRequired();
            this.Property(a => a.Idioma)
                .IsRequired();
            this.Property(a => a.Pais)
                .IsRequired();
            //Asesor tiene muchos conocimientos con muchos Asesores.
            this.HasMany<Conocimiento>(con => con.Conocimientos)
                .WithMany(a => a.Asesores)
                .Map(ase =>
                {
                    ase.MapLeftKey("IDAsesor");
                    ase.MapRightKey("conocimiento_ID");
                    ase.ToTable("AsesorConocimiento");
                });
                

            



        }
    }
}
