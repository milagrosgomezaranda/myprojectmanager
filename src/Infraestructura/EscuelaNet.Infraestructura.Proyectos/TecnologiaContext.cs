﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using EscuelaNet.Dominio.SeedWoork;
using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Infraestructura.Proyectos.EntityTypeConfigurations;

namespace EscuelaNet.Infraestructura.Proyectos
{
    public class TecnologiaContext : DbContext, IUnitOfWork
    {
        public DbSet<Tecnologias> Tecnologias { get; set; }
        public TecnologiaContext() : base("TecnologiaContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TecnologiaEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }

    }
}
