﻿using EscuelaNet.Dominio.Clientes;
using System.Data.Entity.ModelConfiguration;

namespace EsculaNet.Infraestructura.Clientes.EntityTypeConfigurations
{
    public class UnidadesDeNegocioEntityTypeConfiguration :
        EntityTypeConfiguration<UnidadDeNegocio>
    {
        public UnidadesDeNegocioEntityTypeConfiguration()
        {
            this.ToTable("UnidadesDeNegocio");
            this.HasKey<int>(un => un.ID);
            this.Property(un => un.ID)
                .HasColumnName("IDUnidadDeNegocio");
            this.Property(un => un.RazonSocial)                
                .IsRequired();
            this.Property(un => un.ResponsableDeUnidad)
                .IsRequired();
            this.Property(un => un.Cuit)
               .IsRequired();
            this.Property(un => un.EmailResponsable)
                .IsRequired();
            this.Property(un => un.TelefonoResponsable)
                .IsRequired();
            this.Property(un => un.IDCliente)
                .IsRequired();
            this.Property(un => un.Humor)
               .IsRequired();
            this.Property(un => un.AceptarProyecto)
               .IsRequired();

            this.HasMany<Direccion>(un => un.Direcciones)
                .WithRequired(d => d.Unidad)
                .HasForeignKey<int>(d => d.IDUnidadDeNegocio);

            this.HasMany<Solicitud>(un => un.Solicitudes)
                .WithMany(s => s.UnidadesDeNegocio)
                .Map(us =>
                {
                    us.MapRightKey("IDSolicitud");
                    us.MapLeftKey("IDUnidadDeNegocio");
                    us.ToTable("UnidadSolicitud");
                });
           
        }
    }
}