﻿using Autofac;
using Autofac.Integration.Mvc;
using EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura.AutofacModules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Conocimientos.Web.App_Start
{
    public class IoCConfigurator 
    {
        public static void ConfigurarIoC() 
        {
            var builder = new ContainerBuilder(); //Construye nuestros registros. agrega todas las dependencias que voy a ir registrando.
            builder.RegisterControllers(Assembly.GetExecutingAssembly()); // aqui le digo al autofac que me registre todos los controllers q tenga en mi memoria
            builder.RegisterModule(new MediatRModule());
            builder.RegisterModule(new ApplicationModule());
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }
    }
}