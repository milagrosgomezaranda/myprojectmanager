﻿using Autofac;
using EscuelaNet.Aplicacion.Conocimiento.QueryServices;
using EscuelaNet.Dominio.Conocimientos;
using EscuelaNet.Infraestructura.Conocimientos;
using EscuelaNet.Infraestructura.Conocimientos.Repositorios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder) //configura cuando lo que cargue de mis dependencias
        {
            var entorno = ConfigurationManager.AppSettings["Entorno"]; 
            switch (entorno)
            {
                case "Produccion":
                default:
                    var connectionString =
                        ConfigurationManager
                            .ConnectionStrings["ConocimintosContext"].ToString();

                    builder.RegisterType<CategoriaContext>()
                        .InstancePerRequest();


                    builder.Register(c => new CategoriaQuery(connectionString))
                        .As<ICategoriaQuery>()
                        .InstancePerLifetimeScope();

                    builder.RegisterType<CategoriaRepositorio>()
                        .As<ICategoriaRepository>()
                        .InstancePerLifetimeScope();

                    builder.RegisterType<AsesorRepositorio>()
                        .As<IAsesorRepository>()
                        .InstancePerLifetimeScope();
                    
                    break;
                //case "Singleton":
                //default:
                //    builder.RegisterType<EquipoSingletonRepository>()
                //        .As<IEquipoRepository>()
                //        .InstancePerLifetimeScope();
                //    break;
            }

        }
    }
}   

