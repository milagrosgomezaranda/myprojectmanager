﻿using EscuelaNet.Aplicacion.Programadores.QueryModels;
using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Models
{
    public class EquipoSkillModel
    {
        public Equipo Equipo { get; set; }
        public List<Skills> Skills { get; set; }
    }
}