﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Infraestructura.Proyectos;
using EscuelaNet.Infraestructura.Proyectos.Repositorios;

namespace EscuelaNet.Presentacion.Proyectos.Web.Infraestructura.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LineaContext>()
                .InstancePerRequest();
            builder.RegisterType<LineaRepository>()
                .As<ILineaRepository>()
                .InstancePerLifetimeScope();
            base.Load(builder);
        }
    }
}